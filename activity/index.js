let num = 25;
let getCube = num**3;
console.log(`The cube of ${num} is ${getCube}`);

const address = ['Sta. Rosario', 'Cebu City', 'Philippines'];
let [street, city, country] = address;
console.log(`I live at ${street} Street, ${city}, ${country}`);

const animal = {
    givenName: 'Acorn the Hamster',
    type: 'campbell dwarf hamster',
    weight: '50 grams',
    height: '3 inches'
}
let {givenName, type, weight, height} = animal;
console.log(`${givenName} is a ${type}. He weighs ${weight} with a height of ${height}.`);

let numArray = [9, 8, 7, 6, 5];
numArray.forEach(num => console.log(num));

const reduceNumber = (previousValue, currentValue) => previousValue + currentValue;
let sum = numArray.reduce(reduceNumber);
console.log(sum);

class Dog {
    constructor(name, age, breed) {
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
}

let myDog = new Dog('Peanut', 4, 'Beagle');
console.log(myDog);

