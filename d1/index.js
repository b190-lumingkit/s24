// SECTION - Exponent Operator

//pre-es6
/* 
    let/const variableName = Math.pow(number, exponent)
*/
let firstNum = Math.pow(8, 2);
console.log(firstNum);

// es6
/* 
    let/const variableName = number**exponent
*/
let secondNum = 8**2;
console.log(secondNum);

// SECTION - Template Literals
/* 
    - allows to write strings without using the concatination operator (+);
    - helps improve code readability
    - can embed JS expressions
    SYNTAX:
    let variable = `This is a string ${JS expression}`
*/
let userName = "John";

// pre-es6
let message = "Hello " + userName + "! Welcome to programming!";
// Message without template literals: message
console.log('Message without template literals: ' + message);

// es6
message = `Hello ${userName}! Welcome to programming!`;
console.log(`Message with template literals: ${message}`);

// multiple-line
const anotherMessage = `${userName} won the math competition.
He won it by solving the problem 8**2 with the solution of ${8**2}.`

console.log(anotherMessage);

const interestRate = 0.15;
let principal = 1000;

console.log(`Your total interest is: ${principal * interestRate}`);


// SECTION - Array Destructuring
/* 
    - allows us to unpack elements in arrays to distinct variables
    - allows us to name array elements with variables instead of using index numbers
    - helps with code readability
    SYNTAX:
        let/const [varA, varB, varC] = arrayName
        NOTE: be mindful of the variableName and make sure it describes the right element that is stored inside the array
*/
const fullName = ['Juan', 'Dela', 'Cruz'];
// pre-es6
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

// es6
let [firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName}!`);
// SECTION - Object Destructuring
/* 
    - allows us to unpack elements in objects to distinct variables
    - shortens the syntax for accessing the properties from an object
    SYNTAX:
        let/const [propertyA, propertyB, propertyC] = objectName
        NOTE: will return an error if the dev did not use the correct propertyName
*/
// mini-activity
let person = {
    givenName: 'Jane',
    maidenName: 'Dela',
    familyName: 'Cruz'
}
// pre-es6/pre-object-destructuring
console.log(`${person.givenName}\n${person. maidenName}\n${person.familyName}`)
console.log(`Hi there ${person.givenName} ${person. maidenName} ${person.familyName}!`);

// es6 object destructuring
const {givenName, maidenName, familyName} = person;

console.log(`${givenName}\n${maidenName}\n${familyName}`);
console.log(`Hi there ${givenName} ${maidenName} ${familyName}!`);

// usng object destructuring as a parameter of a function
function getFullName({givenName, maidenName, familyName}){
    console.log(`${givenName} ${maidenName} ${familyName}`);
}

getFullName(person);

let pet = {
    name: 'Acorn',
    trick: 'turn around',
    treat: 'sunflower seed'
}

function showPet({name, trick, treat}){
    console.log(`${name}, ${trick}.`);
    console.log(`Good ${trick}!`);
    console.log(`${treat}`);
}

showPet(pet);

// SECTION - Arrow Function
/* 
    - compact alternative syntax to traditional functions
    -
    Syntaxt:
        let/const variableName = (parameter) => { expression };
*/
const hello = () => {
    console.log('Hello World');
}
hello();

// pre-es6
// function printFullName(firstName, middleInitial, lastName) {
//     console.log(`${firstName} ${middleInitial} ${lastName}`);
// }

// arrow function es6
const printFullName = (firstName, middleInitial, lastName) => {
    console.log(`${firstName} ${middleInitial} ${lastName}`);
}
printFullName('Dora', 'D.', 'Explorer');

// arrow function with loops
const students = ["John", "Jane", "Judy"];
// pre-arrow function
students.forEach(function(student){
    console.log(`${student} is a student.`);
})
// es6 arrow function
console.log(`ES6`);
students.forEach(student => console.log(`${student} is a student.`));

// SECTION - implicit return statement
// mini-activity
// pre-es6
/* const add = (x, y) => { return x + y };
console.log(add(99, 888));
console.log(add(782, 1283981)); */

const add = (x, y) => x+y;
console.log(add(99, 888));

// SECTION - Default Argument Value
/* 
    - the "name = 'User'" sets the default value for the function greet() once it is called without any parameters
*/
const greet = (userName = "User") => {
    return `Good morning ${userName}!`;
}

console.log(greet('John'));
// console.log(greet()) - returns 'Good morning User'

// Section - Class-based Object BluePrints
/* 
    Creating a class
    - "constructor" is a special method of a class for creating/initializing an object for that class
    - "this" keyword refers to the properties of an object created from the class; this allow us to reassign values for the properties inside the class
    SYNTAX:
        class className {
            constructor(propertyA, propertyB) {
                this.propertyA = propertyA;
                this.propertyB = propertyB;
            }
            // Getter
            get method() {
                return this.addMethod();
            }
            // Method
            addMethod() {
                return this.propertyA + propertyB;
            }
        }
        console.log(object.method)
*/
class Car{
    constructor(brand, name, year) {
        this.brand = brand;
        this.name = name;
        this.year = year;
    }
};

let car1 = new Car('Toyota', 'Corolla', 1966);
console.log(car1);
console.log(`${car1.brand}\n${car1.name}\n${car1.year}`);

const myNewCar = new Car('Toyota', 'Vios', 2021);
console.log(myNewCar);